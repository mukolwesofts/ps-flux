import dispatcher from '../appDispatcher';
import * as authorApi from '../api/authorApi'
import actionType from './actionTypes'

export function saveAuthor(author) {
    return authorApi.saveAuthor(author)
        .then((savedAuthor) => {
            dispatcher.dispatch({
                actionType: author.id ? actionType.UPDATE_AUTHOR : actionType.CREATE_AUTHOR,
                author: savedAuthor
            });
        });
}

export function loadAuthors() {
    return authorApi.getAuthors()
        .then(authors => {
            dispatcher.dispatch({
                actionType: actionType.LOAD_AUTHORS,
                authors: authors
            });
        });
}

export function deleteAuthor(id) {
    authorApi.deleteAuthor(id)
        .then(() => {
            dispatcher.dispatch({
                actionType: actionType.DELETE_AUTHOR,
                id: id
            });
        });
}