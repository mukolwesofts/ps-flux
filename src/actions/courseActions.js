import dispatcher from '../appDispatcher';
import * as courseApi from '../api/courseApi'
import actionType from './actionTypes'

export function saveCourse(course) { // action creator
    return courseApi.saveCourse(course)
        .then((savedCourse) => {
            // tell all stores that a course was just created
            dispatcher.dispatch({ // this is the action created and passed to dispatch
                actionType: course.id ? actionType.UPDATE_COURSE : actionType.CREATE_COURSE,
                course: savedCourse
            });
        });
}

export function loadCourses() {
    return courseApi.getCourses()
        .then(courses => {
            dispatcher.dispatch({
                actionType: actionType.LOAD_COURSES,
                courses: courses
            });
        });
}

export function deleteCourse(id) {
    courseApi.deleteCourse(id)
        .then(() => {
            dispatcher.dispatch({
                actionType: actionType.DELETE_COURSE,
                id: id
            });
        });
}