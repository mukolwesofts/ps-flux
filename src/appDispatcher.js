import {Dispatcher} from 'flux';

const dispatcher = new Dispatcher(); // create dispatcher
export default dispatcher;