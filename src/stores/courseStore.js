import {EventEmitter} from 'events';
import Dispatcher from '../appDispatcher';
// import actionType from '../actions/actionTypes';

const CHANGE_EVENT = "change";
let _courses = []; // private variable

class CourseStore extends EventEmitter {
    /**
     * 3 must have functions in flux store (look at events nodejs docs)
     * addChangeListener (wraps on)
     * removeChangeListener (wraps removeListener)
     * emitChange (wraps emit)
     */

    // helper methods

    addChangeListener(callback) {
        // allows react components to subscribe to store so they wii be notified
        // when a change occurs
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        // unsubscribe from store
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    getCoursesBySlug(slug) {
        return _courses.find(course => course.slug === slug);
    }

    getCourses() {
        return _courses;
    }
}

const store = new CourseStore();


// register dispatcher
Dispatcher.register(action => {
    switch (action.actionType) {
        case "CREATE_COURSE":
            // from payload that was dispatched line 10
            _courses.push(action.course);

            // anytime store changes we call this method so to tell stores to update
            store.emitChange();
            break;
        case "LOAD_COURSES":
            _courses = action.courses;
            store.emitChange();
            break;
        case "UPDATE_COURSE":
            _courses = _courses.map(course =>
                course.id === action.course.id ? action.course : course
            );
            store.emitChange();
            break;
        case "DELETE_COURSE":
            _courses = _courses.filter(course => course.id !== parseInt(action.id, 10));
            store.emitChange();
            break;
        default:
            // do nothing...store doesnt care
            break;
    }
});

export default store;