import {EventEmitter} from 'events';
import Dispatcher from '../appDispatcher';
// import actionType from '../actions/actionTypes';

const CHANGE_EVENT = "change";
let _authors = [];

class AuthorStore extends EventEmitter {
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    getAuthorsById(id) {
        return _authors.find(author => author.id === parseInt(id));
    }

    getAuthors() {
        return _authors;
    }
}

const store = new AuthorStore();

Dispatcher.register(action => {
    switch (action.actionType) {
        case "LOAD_AUTHORS":
            _authors = action.authors;
            store.emitChange();
            break;
        case "CREATE_AUTHOR":
            _authors.push(action.author);
            store.emitChange();
            break;
        case "UPDATE_AUTHOR":
            _authors = _authors.map(author =>
                author.id === action.author.id ? action.author : author
            );
            store.emitChange();
            break;
        case 'DELETE_AUTHOR':
            _authors = _authors.filter(author => author.id !== parseInt(author.id, 10));
            store.emitChange();
            break;
        default:
            break;
    }
});

export default store;