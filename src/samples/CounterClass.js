import React, {Component} from 'react'

class CounterClass extends Component {
    constructor() {
        super();

        this.state = {
            count: 1
        };
    }

    handleCountAdd = () => {
        this.setState(prevState => ({count: prevState.count + 1}))
    };

    handleCountSub = () => {
        this.setState(prevState => ({count: prevState.count - 1}))
    };

    render() {
        const {count} = this.state;

        return (
            <>
                <h3 className='center'>Class Counter of Life</h3>

                <h2>{count}</h2>
                <button className='btn btn-primary center-block' onClick={this.handleCountAdd}>
                    Add
                </button>

                <button className='btn btn-primary center-block' onClick={this.handleCountSub}>
                    Sub
                </button>
            </>
        )
    }
}


export default CounterClass;