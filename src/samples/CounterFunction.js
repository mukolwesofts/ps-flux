import React, {useState, useEffect} from 'react'

function CounterFunction() {
    const [count, setCount] = useState(1);
    const [time, setTime] = useState(new Date());

    useEffect(() => {
        console.log("useEffect first timer here.")
    }, [count]) // every time count changes

    /**
     * NB: If you pass in an empty array, the effect function
     * is run only on mount — subsequent re-renders don’t
     * trigger the effect function.
     */

    const handleCountAdd = () => {
        setCount(count + 1);
        setTime(new Date());
    };

    const handleCountSub = () => {
        setCount(count - 1);
        setTime(new Date());
    };

    return (
        <>
            <h3 className='center'>Function Counter of Life</h3>

            <h2>{count}</h2>
            <p className="center">
                at: {`${time.getHours()} : ${time.getMinutes()} : ${time.getSeconds()}`}</p>

            <button className='btn btn-primary center-block' onClick={handleCountAdd}>
                Add
            </button>

            <button className='btn btn-primary center-block' onClick={handleCountSub}>
                Sub
            </button>
        </>
    )
}

export default CounterFunction;