import React, {useEffect, useState} from 'react';
import CourseForm from './CourseForm';
import courseStore from '../stores/courseStore';
import {toast} from "react-toastify";
import * as courseActions from '../actions/courseActions';

const ManageCoursePage = props => {
    // use debugger; to stop and debug thus console.log view in sources;

    const [errors, setErrors] = useState({});

    const [courses, setCourses] = useState(courseStore.getCourses);

    const [course, setCourse] = useState({
        id: null,
        slug: '',
        title: '',
        authorId: null,
        category: ''
    });

    useEffect(() => {
        // loads courses whenever there is change in courseStore
        courseStore.addChangeListener(onChange);

        const slug = props.match.params.slug; //to hold slug from url

        if (courses.length === 0) {
            // user loads directly and the flux store has no data yet.
            courseActions.loadCourses();
        } else if (slug) {
            setCourse(courseStore.getCoursesBySlug(slug));
        }

        return () => courseStore.removeChangeListener(onChange);
    }, [courses.length, props.match.params.slug]); // useEffect will run when slug changes

    function onChange() {
        setCourses(courseStore.getCourses());
    }

    function handleChange({target}) {
        // copy course object and set the title i.e. dont set directly
        // [] acts like a computed property i.e. update using name;
        //use setter
        setCourse({
            ...course,
            [target.name]: target.value
        });
    }

    function formIsValid() {
        const _errors = {};

        if (!course.title) _errors.title = "Title is Required";
        if (!course.authorId) _errors.authorId = "Author is Required";
        if (!course.category) _errors.category = "Category is Required";

        setErrors(_errors);

        // is valid if errors is empty
        return Object.keys(_errors).length === 0;
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (!formIsValid()) return;

        courseActions.saveCourse(course)
            .then(() => {
                toast.success('Course Saved');

                props.history.push("/courses");
            });
    }

    return (
        <>
            <h2>Manage Course</h2>

            <CourseForm
                course={course}
                onChange={handleChange}
                onSubmit={handleSubmit}
                errors={errors}
            />
        </>
    );
};

export default ManageCoursePage;