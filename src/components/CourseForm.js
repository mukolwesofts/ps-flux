import React from "react";
import TextInput from './layout/TextInput';
import PropTypes from "prop-types";

// inputs are controlled components that require change handler for props to register user inputs

function CourseForm(props) {
    return (
        <form onSubmit={props.onSubmit}>
            <TextInput
                id="title"
                label='Title'
                onChange={props.onChange}
                name="title"
                value={props.course.title}
                error={props.errors.title}
            />

            <div className="form-group">
                <label htmlFor="author">Author</label>
                <div className="field">
                    <select
                        id="author"
                        name="authorId"
                        onChange={props.onChange}
                        value={props.course.authorId || ""}
                        className="form-control"
                    >
                        <option value=""/>
                        <option value="1">Cory House</option>
                        <option value="2">Scott Allen</option>
                    </select>
                </div>
                {props.errors.authorId && <div className='alert alert-danger'>{props.errors.authorId}</div>}
            </div>

            <TextInput
                id="category"
                label='Category'
                onChange={props.onChange}
                name="category"
                value={props.course.category}
                error={props.errors.category}
            />

            <input type="submit" value="Save" className="btn btn-success"/>
        </form>
    );
}

CourseForm.propTypes = {
    // useful for development to know what component is required
    course: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
};

export default CourseForm;
