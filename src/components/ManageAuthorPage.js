import React, {useEffect, useState} from 'react';
import AuthorForm from './AuthorForm'
import authorStore from "../stores/authorStore";
import * as authorActions from "../actions/authorActions";
import {toast} from "react-toastify";

const ManageCoursePage = props => {
    const [errors, setErrors] = useState({});

    const [authors, setAuthors] = useState(authorStore.getAuthors());

    const [author, setAuthor] = useState({
        id: null,
        name: '',
        professional: '',
    });


    useEffect(() => {
        authorStore.addChangeListener(onChange);

        const id = props.match.params.id;

        if (authors.length === 0) {
            authorActions.loadAuthors();
        } else if (id) {
            setAuthor(authorStore.getAuthorsById(id));
        }

        return () => authorStore.removeChangeListener(onChange);
    }, [authors.length, props.match.params.id]);

    function onChange() {
        setAuthors(authorStore.getAuthors());
    }

    function handleChange({target}) {
        setAuthor({
            ...author,
            [target.name]: target.value
        });
    }

    function formIsValid() {
        const _errors = {};

        if (!author.name) _errors.name = "Name is Required";
        if (!author.professional) _errors.professional = "Professional is Required";

        setErrors(_errors);

        return Object.keys(_errors).length === 0;
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (!formIsValid()) return;

        authorActions.saveAuthor(author)
            .then(() => {
                toast.success('Author Saved');

                props.history.push("/authors");
            });
    }

    return (
        <>
            <h2>Manage Author</h2>

            <AuthorForm
                author={author}
                onChange={handleChange}
                onSubmit={handleSubmit}
                errors={errors}
            />
        </>
    );
};

export default ManageCoursePage;