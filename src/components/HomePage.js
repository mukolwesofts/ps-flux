import React from 'react';
import {Link} from 'react-router-dom'
import CounterClass from '../samples/CounterClass';
import CounterFunction from '../samples/CounterFunction';

function HomePage() {
    return (
        <div className="jumbotron">
            <h1>Pluralsight Admin</h1>
            <p>React Flux, and react Router</p>
            <Link to="about" className="btn btn-primary">About</Link>
            <CounterClass/>
            <CounterFunction/>
        </div>
    )
}

export default HomePage;