import React from 'react';
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Header from './layout/Header'
import HomePage from './HomePage';
import AboutPage from './AboutPage';
import CoursesPage from './CoursesPage';
import AuthorsPage from './AuthorsPage';
import NotFoundPage from './NotFoundPage';
import ManageCoursePage from './ManageCoursePage';
import ManageAuthorPage from './ManageAuthorPage';
import {Redirect, Route, Switch} from 'react-router-dom'

function App() {
    return (
        <div className="container-fluid">
            <ToastContainer autoclose={3000} hideProgressBar/>

            <Header/>

            <Switch>
                <Route path="/" exact component={HomePage}/>
                <Route path="/courses" component={CoursesPage}/>
                <Route path="/authors" component={AuthorsPage}/>
                <Route path="/about" component={AboutPage}/>
                <Route path="/course/:slug" component={ManageCoursePage}/>
                <Route path="/course/" component={ManageCoursePage}/>
                <Route path="/author/:id" component={ManageAuthorPage}/>
                <Route path="/author/" component={ManageAuthorPage}/>
                <Redirect from='/about-page' to='about'/>
                <Route component={NotFoundPage}/> {/*default*/}
            </Switch>
        </div>
    )
}

export default App;