import React from 'react';
import TextInput from './layout/TextInput';
import PropTypes from "prop-types";
import CourseForm from "./CourseForm";

function AuthorForm(props) {
    return (
        <>
            <form onSubmit={props.onSubmit}>
                <TextInput
                    id="name"
                    label='Name'
                    onChange={props.onChange}
                    name="name"
                    value={props.author.name}
                    error={props.errors.name}
                />

                <TextInput
                    id="professional"
                    label='Professional'
                    onChange={props.onChange}
                    name="professional"
                    value={props.author.professional}
                    error={props.errors.professional}
                />

                <input type="submit" value="Save" className="btn btn-success"/>
            </form>
        </>
    );
}

CourseForm.propTypes = {
    author: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
};

export default AuthorForm;


