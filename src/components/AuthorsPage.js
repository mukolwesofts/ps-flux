import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import AuthorList from "./AuthorList";
import authorStore from "../stores/authorStore";
import {deleteAuthor, loadAuthors} from "../actions/authorActions";

function AuthorsPage() {
    const [authors, setAuthors] = useState(authorStore.getAuthors);

    useEffect(() => {
        authorStore.addChangeListener(onChange);

        if (authorStore.getAuthors().length === 0) loadAuthors();

        return () => authorStore.removeChangeListener(onChange);
    }, []);

    function onChange() {
        setAuthors(authorStore.getAuthors())
    }

    return (
        <>
            <h2>Authors Page</h2>

            <Link to='/author' className='btn btn-primary'>
                Add Author
            </Link>
            <br/>
            <AuthorList authors={authors} deleteAuthor={deleteAuthor}/>
        </>
    );
}

export default AuthorsPage;