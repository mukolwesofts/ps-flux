import React from 'react'
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

function AuthorList(props) {
    return (
        <table className="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Profession</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            {props.authors.map(author => {
                return (
                    <tr key={author.id}>
                        <td>{author.id}</td>
                        <td>{author.name}</td>
                        <td>{author.professional}</td>
                        <td>
                            <Link to={"/author/" + author.id} className="btn btn-info">
                                VIEW
                            </Link>
                        </td>
                        <td>
                            <button className="btn btn-danger"
                                    onClick={() => props.deleteAuthor(author.id)}>
                                DELETE
                            </button>
                        </td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    );
}

AuthorList.propTypes = {
    deleteAuthor: PropTypes.func.isRequired,
    authors: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        professional: PropTypes.string.isRequired,
    })).isRequired,
};

export default AuthorList;

