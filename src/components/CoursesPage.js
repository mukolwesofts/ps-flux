import React, {useEffect, useState} from 'react';
import courseStore from '../stores/courseStore';
import CourseList from './CourseList'
import {Link} from "react-router-dom";
import {deleteCourse, loadCourses} from "../actions/courseActions";

function CoursesPage() {
    const [courses, setCourses] = useState(courseStore.getCourses);

    useEffect(() => {
        courseStore.addChangeListener(onChange);
        if (courseStore.getCourses().length === 0) loadCourses(); // lazy loading

        // use effect return statement can be used to unmount stuff by returning a function
        return () => courseStore.removeChangeListener(onChange); // cleanup on unmount
    }, []); // empty array mean we want it to run once


    function onChange() {
        setCourses(courseStore.getCourses())
    }

    return (
        <>
            <h2>Courses</h2>

            <Link to='/course' className='btn btn-primary'>
                Add Course
            </Link>
            <br/>
            <CourseList courses={courses} deleteCourse={deleteCourse}/>
        </>
    )
}

export default CoursesPage;